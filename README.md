# AI



## Getting started

Vorab wird Java benötigt. Das Programm wurde für JRE 1.8.0.372 Umgebung entwickelt.
Es sollte aber auch in anderen Umgebungen lauffähig sein.
Tested on:
Windows 10 64bit:
JRE 1.8.0.362
JRE 17.0.6
JRE 20.0.2

Desweiteren wird ein openAI.com account benötigt (www.openai.com)
Ich Empfehle ein bestehendes Microsoft Konto zu verwenden.

Es muss eine gültige Bezahlmethode hinterlegt werden (https://platform.openai.com/account/billing/payment-methods)
und Idealerweise lädt man seine Credits im Voraus um ca. $2 auf (https://platform.openai.com/account/billing/overview und dort "buy credits")

Danach benötigt man einen API-Key, dieser wird hier erstellt:
https://platform.openai.com/api-keys

## Download

[AI for JRE 8 and above](https://grebe.hamburg/AI-for-JRE8andAbove.zip)

[AI for JRE 1.8.0 and below](https://grebe.hamburg/AI.zip)


## Description
Dieses Tool wurde entwickelt um die API von OpenAI zu nutzen. So lassen sich verschiedene Sprachmodelle nutzen und man Bezahlt nur das, was man tatsächlich verbraucht. Kein Abo, keine Pflicht zum Kauf, es wird nur das berechnet, was man auch tatsächlich benötigt.
die Gültige Preisliste für Anfragen findet sich hier:
https://openai.com/pricing


## Installation
Vorab wird Java benötigt. Das Programm wurde für JRE 1.8.0.372 Umgebung entwickelt.
Es sollte aber auch in anderen Umgebungen lauffähig sein.
Tested on:
Windows 10 64bit:
JRE 1.8.0.362
JRE 17.0.6
JRE 20.0.2

Desweiteren wird ein openAI.com account benötigt (www.openai.com)
Ich Empfehle ein bestehendes Microsoft Konto zu verwenden.

Es muss eine gültige Bezahlmethode hinterlegt werden (https://platform.openai.com/account/billing/payment-methods)
und Idealerweise lädt man seine Credits im Voraus um ca. $2 auf (https://platform.openai.com/account/billing/overview und dort "buy credits")

Danach benötigt man einen API-Key, dieser wird hier erstellt:
https://platform.openai.com/api-keys

Nach Programmstart lässt sich der API-Key über die Menuleiste Edit -> Settings
bei API-Key eintragen.
Falls benötigt, lassen sich auch Proxy Einstellungen tätigen
(Bei der Verwendung von Benutzer & Passwort, muss das Programm neugestartet werden)

unter dem Reiter "Advanced" lässt sich das gültige Sprachmodell auswählen
(Sobald $1 kredit gekauft wurde, sind die GPT-4 Modelle sowie Dall-E-3 anwählbar)

## Usage
Im Textfeld "Assistenten Aufgabe" lässt sich eine Rolle für den Assistenten einstellen. Standardmäßig "You are a helpful assistant"
Dort lassen sich gut vorgefertige Prompts für spezielle Aufgaben eintragen. Näheres Dazu findet sich im OpenAI Discord.
Mittels der Tabulator-Taste wird die getätigte Eingabe übernommen und man landet automatisch im Eingabetextfeld.

Das untere Fenster ist das Eingabetextfeld. Hier stellt man seine Fragen, lässt seine Bilder generieren etc. pp

Setzt man den Haken bei "Dall-E-3", wird die nächste Eingabe im Eingabetextfeld an den Bildgenerator weitergeleitet und es werden Standardmäßig 2 Bilder generiert.
Die Einstellungen für Dall-E-3 lassen sich über die Menuleiste Edit -> Dall-E-Settings tätigen.
Dort kann man das Format, die Anzahl der zu genrierenden Bilder, den Stiel, sowie HD Auflösung einstellen. Die Einstellungen wirken sich auf die Preise für eine Bildgenrierung aus, also beachtet bitte die Preisliste (https://openai.com/pricing)


## Support


## Roadmap


## License
Licensed under GNU GPL

## Project status
Active in Development and Improvement
