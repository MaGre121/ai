package handler;

import backend.Settings;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;


public class ConnectionHandler {
//    private static HttpURLConnection con;

/*    private static void openConnection(String url) throws IOException {
        con = Settings.getProxy() != null ? (HttpURLConnection) new URL(url).openConnection(Settings.getProxy()) : (HttpURLConnection) new URL(url).openConnection();
    }*/

    private static void setRequestHeaders(HttpURLConnection con, boolean contentType, String requestMethod) throws ProtocolException {
        con.setRequestMethod(requestMethod);
        if (contentType)
            con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Authorization", "Bearer " + Settings.getApiKey());
    }

    private static void closeConnection(HttpURLConnection con) {
        if (con != null) con.disconnect();
    }

    public static JsonObject connect(JsonObject jsonObject, String url) {
        return connect(jsonObject, url, true, "POST");
    }

    public static JsonObject connect(JsonObject jsonObject, String url, boolean contentType, String requestMethod) {
        HttpURLConnection con = null;
        try {
//            openConnection(url);
            con = Settings.getProxy() != null ? (HttpURLConnection) new URL(url).openConnection(Settings.getProxy()) : (HttpURLConnection) new URL(url).openConnection();
            setRequestHeaders(con, contentType, requestMethod);

            if (jsonObject != null) {
                con.setDoOutput(true);
//                con.getOutputStream().write(jsonObject.toString().getBytes(StandardCharsets.UTF_8));
                try (OutputStream outputStream = con.getOutputStream()) {
                    outputStream.write(jsonObject.toString().getBytes(StandardCharsets.UTF_8));
                }
            }

            Gson gson = new Gson();
            JsonObject output;
            try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) { //Setting Charset to UTF-8 because Java 1.8* needs this :S
                output = gson.fromJson(in, JsonObject.class);
                return output;
            } catch (Exception e) {
                try (BufferedReader errorIn = new BufferedReader(new InputStreamReader(con.getErrorStream()))) {
                    String errorResponse = errorIn.lines().collect(Collectors.joining());
                    System.out.println("HTTP Response Code: " + con.getResponseCode());
                    System.out.println("Error Response: " + errorResponse);
                } catch (Exception err) {
                    err.printStackTrace();
                }
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeConnection(con);
        }
        return null;
    }

    public static InputStream downloadImage(URL url){
        URLConnection con = null;
        try {
            con = Settings.getProxy() != null ? url.openConnection(Settings.getProxy()) : url.openConnection();
            return con.getInputStream();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public static JsonObject multipartConnect(String byteCode, String prompt, int n, String size, String url, String imgFormat) {
        HttpURLConnection con = null;
        try {
//            openConnection(url);
            con = Settings.getProxy() != null ? (HttpURLConnection) new URL(url).openConnection(Settings.getProxy()) : (HttpURLConnection) new URL(url).openConnection();
            // Erzeuge den multipart-http header
            String boundary = "------------------------" + Long.toHexString(System.currentTimeMillis());
//            String boundary = new String(("------------------------" + Long.toHexString(System.currentTimeMillis())).getBytes(), StandardCharsets.UTF_8);
            String CRLF = "\r\n";

//            con = Settings.getProxy() != null ? (HttpURLConnection) new URL(url).openConnection(Settings.getProxy()) : (HttpURLConnection) new URL(url).openConnection();

            // Setze die Request-Methode, den Content-Type und den Authorization-Header
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            con.setRequestProperty("Authorization", "Bearer " + Settings.getApiKey());
            con.setRequestProperty("Accept", "*/*");
            con.setDoOutput(true);

            try (DataOutputStream out = new DataOutputStream(con.getOutputStream())) {
                out.writeBytes("--" + boundary + CRLF);

                out.writeBytes("Content-Disposition: form-data; name=\"image\"; filename=\"" + CRLF);
                out.writeBytes("Content-Type: image/png" + CRLF);
                out.writeBytes(CRLF + byteCode + CRLF);

                if (!prompt.equals("")) {
                    out.writeBytes("--" + boundary + CRLF);
                    out.writeBytes("Content-Disposition: form-data; name=\"prompt\"" + CRLF);
                    out.writeBytes(CRLF + prompt + CRLF);
                }

                out.writeBytes("--" + boundary + CRLF);
                out.writeBytes("Content-Disposition: form-data; name=\"n\"" + CRLF);
                out.writeBytes(CRLF + n + CRLF);

                out.writeBytes("--" + boundary + CRLF);
                out.writeBytes("Content-Disposition: form-data; name=\"size\"" + CRLF);
                out.writeBytes(CRLF + size + CRLF);

                out.writeBytes("--" + boundary + CRLF);
                out.writeBytes("Content-Disposition: form-data; name=\"response_format\"" + CRLF);
                out.writeBytes(CRLF + imgFormat + CRLF);

                out.writeBytes("--" + boundary + "--" + CRLF);
            }

            // Sende den Request und erhalte die Response
            Gson gson = new Gson();
            JsonObject output;
            try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) { //Setting Charset to UTF-8 because Java 1.8* needs this :S
                output = gson.fromJson(in, JsonObject.class);
                return output;
            } catch (Exception e) {
                try (BufferedReader errorIn = new BufferedReader(new InputStreamReader(con.getErrorStream()))) {
                    String errorResponse = errorIn.lines().collect(Collectors.joining());
                    System.out.println("HTTP Response Code: " + con.getResponseCode());
                    System.out.println("Error Response: " + errorResponse);
                } catch (Exception err) {
                    err.printStackTrace();
                }
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeConnection(con);
        }
        return null;
    }
}
