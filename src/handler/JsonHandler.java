package handler;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JsonHandler {
    private static List<JsonElement> searchList;

/*    public static List<String> getKeyValues(JsonObject jsonObject, String searchKey){
        searchList = new ArrayList<>();
        searchJsonObject(jsonObject, searchKey);
        return searchList;
    }*/
    public static List<JsonElement> getElements(JsonObject jsonObject, String searchKey){
        searchList = new ArrayList<>();
        searchJsonObject(jsonObject, searchKey);
        return searchList;
    }

    /**
     * Searches for a specific key in a JsonObject recursively.
     * @param jsonObject The JsonObject to search in.
     * @param searchKey The key to search for.
     */
    private static void searchJsonObject(JsonObject jsonObject, String searchKey) {
        for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
            String key = entry.getKey();
            JsonElement value = entry.getValue();

            if (key.equals(searchKey)) {
//                searchList.add(entry.getValue().getAsString());
                searchList.add(entry.getValue());
            }

            if (value.isJsonObject()) {
                searchJsonObject(value.getAsJsonObject(), searchKey);
            } else if (value.isJsonArray()) {
                searchJsonArray(value.getAsJsonArray(), searchKey);
            }
        }
    }

    private static void searchJsonArray(JsonArray jsonArray, String searchKey) {
        for (JsonElement element : jsonArray) {
            if (element.isJsonObject()) {
                searchJsonObject(element.getAsJsonObject(), searchKey);
            } else if (element.isJsonArray()) {
                searchJsonArray(element.getAsJsonArray(), searchKey);
            }
        }
    }
}
