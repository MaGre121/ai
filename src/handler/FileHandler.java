package handler;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import backend.DallEObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.w3c.dom.*;

import java.io.*;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.List;

public class FileHandler {
    private static Path projectPath = Paths.get(System.getProperty("user.home") + "/documents/AI");
    private static Path imagePath;
    private static Path histPath;
    private static File projectFile;
    private static File histFile;
    private static String projectName;

    public static void createProject(File file) {
        setProjectVariables(file);
        try {
            Files.createDirectories(projectPath); //Create Project Folder
            Files.createDirectory(histPath); //Create ChatHistory Folder
            Files.createDirectory(imagePath); //Create Images Folder
            initializeProject();
        } catch (IOException e) {
//            e.printStackTrace();
        }
    }

    public static void reset(){
        projectPath = Paths.get(System.getProperty("user.home") + "/documents/AI");
        projectName = null;
        projectFile = null;
        imagePath = null;
        histPath = null;
        histFile = null;
    }

    private static void setProjectVariables(File file) {
        projectFile = file;
        projectName = file.getName().replaceAll("\\.xml", "");
        projectPath = Paths.get(file.toPath().getParent() + "/" + projectName);
        imagePath = Paths.get(projectPath + "/" + "Images" + "/");
        histPath = Paths.get(projectPath + "/" + "Chat");
        histFile = Paths.get(histPath + "/" + projectName + ".html").toFile();
    }

    public static String saveConversationAsHTML(JsonArray messagesArray) {
        StringBuilder sb = new StringBuilder("<html>\n<body>\n");
        for (JsonElement jsonElement : messagesArray) {
            String role = jsonElement.getAsJsonObject().get("role").getAsString();
            String content = jsonElement.getAsJsonObject().get("content").getAsString().replace("\n", "<br>");
            String style = "";
            if (role.equals("user")) {
                style = "green";
            } else if (role.equals("assistant")) {
                style = "red";
            } else if (role.equals("system")) {
                style = "orange";
            }
            sb.append("<p>\n\t<b style=\"color:").append(style).append("\">").append(role).append(":</b><br>\n\t")
                    .append(content).append("<br>\n</p>\n");
        }
        sb.append("</body>\n</html>");

        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(Files.newOutputStream(histFile.toPath()), StandardCharsets.UTF_8))) {
            writer.write(sb.toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return histFile.getPath();
    }

    public static void saveProject() {

    }

    public static void saveImage(String image, int name) {
        String fileName = name + ".png";
        try {
            byte[] imageBytes = Base64.getDecoder().decode(image);
            Files.write(Paths.get(imagePath + "/" + fileName), imageBytes);

        } catch (IOException e) {
            System.out.println("An error occurred while saving the image.");
            e.printStackTrace();
        }
    }

    public static String downloadImage(URL url) {
        File outFile = null;
        try {
            if (imagePath == null) {
                outFile = File.createTempFile("dalle", ".png");
            } else {
                int name = url.hashCode() & Integer.MAX_VALUE;
                outFile = Paths.get(imagePath + "/" + name + ".png").toFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("FilePath not proper set");
        }

        try (InputStream in = ConnectionHandler.downloadImage(url)) {
            assert outFile != null;
            try (FileOutputStream fos = new FileOutputStream(outFile);
                 BufferedOutputStream bout = new BufferedOutputStream(fos, 1024)) {

                byte[] data = new byte[1024];
                int count;
                while ((count = in.read(data, 0, 1024)) != -1) {
                    bout.write(data, 0, count);
                }
            }
        } catch (IOException e) {
            System.err.println("Error occurred during file download: " + e.getMessage() + "\t" + url);
        }

        return outFile.getAbsolutePath();
    }

    public static String loadImage(Path path) {
        StringBuilder sb = new StringBuilder();
        try {
            byte[] bytes = Files.readAllBytes(path);

            for (byte b : bytes) {
                int unsignedByte = b & 0xFF;
                sb.append((char) unsignedByte);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static Document loadProject(File pathToFile) {
        if (projectFile == null)
            setProjectVariables(pathToFile);

        Document doc;

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            doc = docBuilder.parse(pathToFile);
            doc.getDocumentElement().normalize();
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private static String getCurrentDate() {
        LocalDateTime date = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmm");
        return date.format(formatter);
    }

    public static File getCurrentProject() {
        return projectFile;
    }

    public static Path getDefaultProjectPath() {
        return projectPath;
    }

    private static void initializeProject() {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // Root element
            Document doc;
            Element rootElement;
            File xmlFile = projectFile;

            doc = docBuilder.newDocument();
            rootElement = doc.createElement("AIProject");
            doc.appendChild(rootElement);

            //Projectname Element
            Element proName = doc.createElement("projectName");
            proName.appendChild(doc.createTextNode(projectName));
            rootElement.appendChild(proName);

            //Project Conversation History
            Element histElem = doc.createElement("historyFile");
            histElem.appendChild(doc.createTextNode(histFile.getAbsolutePath()));
            rootElement.appendChild(histElem);


            // Write the content into XML file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(xmlFile);

            transformer.transform(source, result);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    only Call this Function if savingActive is true!
    public static void addImagesToProjectXML(DallEObject dallEObject) {
        Document doc = loadProject(projectFile);
        assert doc != null;
        try {
/*            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // Root element
            Document doc;

            File xmlFile = projectFile;

            doc = docBuilder.parse(xmlFile);*/

            Element rootElement;
            rootElement = doc.getDocumentElement();
/*            //Projectname Element
            Element proName = doc.createElement("projectName");
            proName.appendChild(doc.createTextNode(projectName));
            rootElement.appendChild(proName);*/

            // Request element
            Element request = doc.createElement("DallEObject");
            rootElement.appendChild(request);

            // Prompt element
            Element promptElement = doc.createElement("Prompt");
            promptElement.appendChild(doc.createTextNode(dallEObject.getPrompt()));
            request.appendChild(promptElement);

            // n element
            Element nElement = doc.createElement("n");
            nElement.appendChild(doc.createTextNode(String.valueOf(dallEObject.getN())));
            request.appendChild(nElement);

            // size element
            Element sizeElement = doc.createElement("size");
            sizeElement.appendChild(doc.createTextNode(dallEObject.getSize()));
            request.appendChild(sizeElement);

            // response_format element
            Element responseFormatElement = doc.createElement("response_format");
            responseFormatElement.appendChild(doc.createTextNode(dallEObject.getResponseFormat()));
            request.appendChild(responseFormatElement);

            Element qualityElement = doc.createElement("hd");
            qualityElement.appendChild(doc.createTextNode(String.valueOf(dallEObject.getQuality())));
            request.appendChild(qualityElement);

            Element styleElement = doc.createElement("style");
            styleElement.appendChild(doc.createTextNode(dallEObject.getStyle()));
            request.appendChild(styleElement);

            // Reply element
            Element reply = doc.createElement("Reply");
            request.appendChild(reply);

            // image elements
            for (int i = 0; i < dallEObject.getN(); i++) {
                Element imageElement = doc.createElement("image");
                imageElement.appendChild(doc.createTextNode(dallEObject.getFileNames().get(i)));
                reply.appendChild(imageElement);

                Element revisedPromptElement = doc.createElement("revisedPrompt");
                revisedPromptElement.appendChild(doc.createTextNode(dallEObject.getRevisedPrompts().get(i)));
                reply.appendChild(revisedPromptElement);
            }


            // Write the content into XML file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(projectFile);

            transformer.transform(source, result);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void moveImagesFromTempToProject(DallEObject dallEObject) {
        for (String fileName : dallEObject.getFileNames()) {
            if (fileName.contains(System.getProperty("java.io.tmpdir"))) {
                Path oldPath = Paths.get(fileName);
                String newName = oldPath.getFileName().toString();
                Path newPath = Paths.get(imagePath + "/" + newName);
                try {
                    Files.move(oldPath, newPath);
                } catch (Exception e) {
                    System.err.println("Error while moving Files " + e.getMessage());
                    System.err.println(FileHandler.class);
                }
                dallEObject.replaceFileName(fileName, String.valueOf(newPath));
            }
        }
    }
}
