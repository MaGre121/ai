package gui;

import backend.Settings;
import backend.enums.DallEStyleModes;
import backend.enums.DalleSizes;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class DallEGUISettings {
    public ComboBox<String> fx_dalle_sizes;
    public ComboBox<String> fx_dalle_style;
    public ComboBox<Integer> fx_dalle_numImages;
    public CheckBox fx_useDallEHD;
    public VBox fx_DallEsettings_root;
    private boolean useDalleHD;
    private Stage settingsStage;

    public void initialize(){
//        settingsStage = (Stage)fx_DallEsettings_root.getScene().getWindow();
        if (Settings.getDallesize() == null && Settings.getDallestyle() == null)
            Settings.writeDallePoperties("1024x1024", "natural", false, 2);
        try {
            fx_dalle_sizes.setValue(Settings.getDallesize());
            fx_dalle_style.setValue(Settings.getDallestyle());
            fx_dalle_numImages.setValue(Settings.getDallenum());
            fx_useDallEHD.selectedProperty().setValue(Settings.getDallequality());


            for (DalleSizes size: DalleSizes.values()){
                fx_dalle_sizes.getItems().add(size.getDisplayValue());
            }
            for (DallEStyleModes style : DallEStyleModes.values()){
                fx_dalle_style.getItems().add(style.getDisplayValue());
            }
            for (int i = 1; i < 11; i++) {
                fx_dalle_numImages.getItems().add(i);
            }
            fx_useDallEHD.selectedProperty().addListener((observable, oldVal, newVal) -> {
                if (newVal)
                    useDalleHD = true;
                else
                    useDalleHD = false;
            });
        } catch (Exception e){
            System.out.println("nothing to see here");
        }
    }

    public void onSendSettingsCancel() {
        settingsStage = (Stage)fx_DallEsettings_root.getScene().getWindow();
        settingsStage.close();
    }

    public void onSendSettingsSave() {
        Settings.writeDallePoperties(fx_dalle_sizes.getValue(), fx_dalle_style.getValue(), useDalleHD, fx_dalle_numImages.getValue());
        Settings.update();
        onSendSettingsCancel();
    }

}
