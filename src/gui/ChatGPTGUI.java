package gui;


import handler.FileHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.TextField;

import backend.*;
import utils.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map;

/**
 * @author Matthias Grebe
 * @version 1.1
 */

public class ChatGPTGUI {

    //FXML Elements
    @FXML
    private AnchorPane fx_root;
    @FXML
    private VBox fx_subRoot;
    @FXML
    private MenuBar fx_menu;
    @FXML
    private TextField fx_role;
    @FXML
    private Label fx_topicLabel;
    @FXML
    private CheckBox fx_useDallE;
    @FXML
    private ScrollPane fx_scrollPane;
    @FXML
    private TextArea insert;
    @FXML
    private TextFlow fx_output;
    @FXML
    private Label fxLabelAIRole;
    @FXML
    private HBox fxHboxButton;


    //Engine Elements
    private ChatGPT cgpt;
    private DallE dalle;
    private boolean savingActive = false;
    private boolean useDalle;

    public ChatGPTGUI() {
        this.cgpt = new ChatGPT();
        this.dalle = new DallE();
    }

    //    doing some layouting here
    public void initialize() {

        fx_subRoot.prefWidthProperty().bind(fx_root.widthProperty().subtract(5));
        fx_subRoot.prefHeightProperty().bind(fx_root.heightProperty().subtract(5));
        fx_menu.prefWidthProperty().bind(fx_subRoot.widthProperty());
        fx_topicLabel.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(20));
        fx_role.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(fxLabelAIRole.widthProperty()));
        fx_scrollPane.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(20));
        fx_scrollPane.prefHeightProperty().bind(fx_subRoot.heightProperty().divide(1.8));
        fx_output.prefWidthProperty().bind(fx_scrollPane.widthProperty().subtract(20));
        insert.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(20));
        insert.maxWidth(fx_root.widthProperty().get());
        fxHboxButton.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(20));
        fx_role.setFont(new Font("UTF-8", 12));

        fx_role.setText(cgpt.getRole());
        fx_useDallE.selectedProperty().addListener((observable, oldVal, newVal) -> {
            if (newVal)
                useDalle = true;
            else
                useDalle = false;
        });
    }


    @FXML
    protected void onSendButtonClick() {
        String linkContent = "";
        String insertedText = insert.getText();
        if (insertedText.contains("https://")) {
            linkContent = StripLinkFromText.getLink(insertedText);
            insertedText = insertedText + "\n" + linkContent;
        }

        CustomTextArea inText = new CustomTextArea(insertedText);
        inText.prefWidthProperty().bind(fx_output.widthProperty().subtract(5));
        // asking chatGPT or DallE here
        if (useDalle) {
            Text ROLE = new Text("asking DallE...");
            ROLE.setStyle("-fx-font-weight:bold;" +
                    "-fx-fill:orange;");
            System.out.println("asking DallE...");
            dalle.generateImage(insertedText, savingActive);
            fx_output.getChildren().addAll(inText, ROLE, createImageGridPane());
        } else {

            cgpt.askChatGPT(insertedText);
            String outputText = cgpt.getAnswer();
            CustomTextArea outText = new CustomTextArea(outputText);
            outText.prefWidthProperty().bind(fx_output.widthProperty().subtract(5));
            Text USER = new Text("\nUser:\n");
            Text AI = new Text("\nChatGPT:\n");
            USER.setStyle("-fx-font-weight:bold;" +
                    "-fx-fill:red;");
            AI.setStyle("-fx-font-weight:bold;" +
                    "-fx-fill:green;");

            fx_output.getChildren().addAll(USER, inText, AI, outText);
            if (savingActive) {
                cgpt.saveConversation();
            }
        }
        insert.clear();

    }

    //    Clear all windows and functions
    public void onClearButtonClick() {
        cgpt = new ChatGPT();
        dalle = new DallE();
        FileHandler.reset();
        insert.clear();
        savingActive = false;
        fx_output.getChildren().clear();
        fx_role.setText(cgpt.getRole());

    }

    public void onSaveButtonClick() {
        if (!savingActive)
            onMenuSaveProject();

        Text ROLE = new Text("Saved conversation to:\t" + cgpt.saveConversation());
        ROLE.setStyle("-fx-font-weight:bold;" +
                "-fx-fill:orange;");
        fx_output.getChildren().add(ROLE);
    }

    public void onRoleChange(KeyEvent key) {
        if (key.getCode() == KeyCode.TAB) {
            Text ROLE = new Text("AI Role set to:\t" + fx_role.getText());
            cgpt.clearHistory();
            cgpt.changeRole(fx_role.getText());
            ROLE.setStyle("-fx-font-weight:bold;" +
                    "-fx-fill:orange;");
            fx_output.getChildren().add(ROLE);
        }
    }

    public void onMenuCloseButton() {
        cgpt.exitProgram();
    }

    public void onMenuSettingsButton() throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ChatGPT-Settings.fxml"));
        Parent root = fxmlLoader.load();
//        URL url = getClass().getResource("/ChatGPT-Settings.fxml");
//        Parent root = FXMLLoader.load(url);
        Scene scene = new Scene(root);
        stage.setTitle("ChatGPT Settings");
        stage.setScene(scene);
        stage.setResizable(true);
        ChatGPTGUISettings controller = fxmlLoader.getController();
        controller.setRootStage(stage);

        stage.showAndWait();
    }

    public void onMenuAboutButton() throws IOException {
        Stage stage = new Stage();
        URL url = getClass().getResource("/ChatGPT-Help.fxml");
        assert url != null;
        Parent root = FXMLLoader.load(url);
        Scene scene = new Scene(root, 500, 250);
        stage.setTitle("ChatGPT About");
        stage.setScene(scene);
        stage.show();
    }

    public void onMenuSaveProject() {
        Stage rootStage = (Stage) fx_root.getScene().getWindow();
        FileChooser fc = new FileChooser();

        fc.setInitialDirectory(FileHandler.getDefaultProjectPath().toFile());
        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("XML-File", "*.xml")
        );
        File selectedFile = fc.showSaveDialog(rootStage);
        if (selectedFile != null)
            FileHandler.createProject(selectedFile);

        cgpt.saveConversation();
        dalle.saveAllImages();
        savingActive = true;
    }

    public void onMenuDallESettingsButton() throws IOException {
        Stage stage = new Stage();
        stage.setAlwaysOnTop(true);
        URL url = getClass().getResource("/DallE-Settings.fxml");
        assert url != null;
        Parent root = FXMLLoader.load(url);
        Scene scene = new Scene(root, 500,278.0);
        stage.setTitle("Dall-E Settings");
        stage.setScene(scene);
        stage.showAndWait();
    }

    public void onMenuLoadProject() {
        onClearButtonClick();
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(FileHandler.getDefaultProjectPath().toFile());
        File projectFile = fc.showOpenDialog(new Stage());

        dalle.loadProject(projectFile);
        for (DallEObject dallEObject : dalle.getDallEObjects()) {
            fx_output.getChildren().add(createImageGridPane(dallEObject));
        }
        savingActive = true;
    }

    private GridPane createImageGridPane(DallEObject dallEObject) {
        // Get the current width of the ScrollPane to determine image widths
        double scrollPaneWidth = fx_scrollPane.getWidth();

        // Create a new GridPane object
        GridPane gp = new GridPane();
        gp.setAlignment(Pos.CENTER);
        gp.setPadding(new Insets(10));
        gp.setHgap(5);
        gp.setVgap(5);

        // Retrieve the image information map from the DallEObject
        Map<Image, String> imageInfo = dalle.getImageFromObject(dallEObject);

        // Set the column constraints: at most 2 images per row
        int maxImagesPerRow = 2;
        double imageFitWidth = scrollPaneWidth / maxImagesPerRow - (10 + gp.getHgap()) * (maxImagesPerRow - 1);

        for (int col = 0; col < maxImagesPerRow; col++) {
            ColumnConstraints column = new ColumnConstraints(imageFitWidth);
            gp.getColumnConstraints().add(column);
        }

        // Add images to the grid pane
        int row = 1; // Start from the second row (row 1), since the first row is for the header
        int col = 0;
        for (Map.Entry<Image, String> entry : imageInfo.entrySet()) {
            // Create and configure the ImageView
            ImageView imageView = new ImageView(entry.getKey());
            imageView.setPreserveRatio(true);
            imageView.setFitWidth(imageFitWidth / 2);

            // Center the image if it's the only one
            if (imageInfo.size() == 1) {
                gp.setHalignment(imageView, HPos.CENTER);
            }

            // Add event handlers to the ImageView
            imageView.setOnMouseClicked(event -> {
                if (event.getClickCount() == 1) {
                    insert.setText(entry.getValue());
                    insert.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(20));
                } else if (event.getClickCount() == 2) {
                    openImageInNewWindow(entry.getKey());
                }
            });

            // Add the ImageView to the grid pane
            gp.add(imageView, col, row, 1, 1);

            // Update column and row indices
            col++;
            if (col >= maxImagesPerRow) {
                col = 0;
                row++;
            }
        }

        // If there's one image to be centered, adjust the row span
        if (imageInfo.size() == 1) {
            // Get the node (TextField) added in the first row and span 2 columns
            Node header = gp.getChildren().get(0); // Assuming header is the first child added
            gp.setColumnSpan(header, maxImagesPerRow);
        }

        // Return the configured grid pane
        return gp;
    }

    // Method to open an image in a new window
    private void openImageInNewWindow(Image image) {
        ImageView imageView = new ImageView(image);

        // Set up the Stage and Scene
        Stage stage = new Stage();
        stage.setTitle("Image View");
        Pane pane = new Pane(imageView);
        Scene scene = new Scene(pane);

        stage.setScene(scene);
        stage.show();
    }

    private GridPane createImageGridPane(){
         return createImageGridPane(dalle.getLatestObject());

    }
}
