/*
package gui;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.TextFlow;
import net.mod.dalle.DallE;
import net.mod.dalle.FileHandler;
import net.mod.dalle.Request;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class DallEGUI {
    // Dalle implementation
    private DallE dallE;

    //FXML Elements
    @FXML
    private AnchorPane fx_root;
    @FXML
    private VBox fx_subRoot;
    @FXML
    private MenuBar fx_menu;
    @FXML
    private Label fx_topicLabel;
    @FXML
    private ScrollPane fx_scrollPane;
    @FXML
    private TextArea fx_description;
    @FXML
    private TextFlow fx_output;
    @FXML
    private HBox fxHboxButton;

    public DallEGUI() {
        this.dallE  = new DallE();
    }

    public void initialize(){
        fx_subRoot.prefWidthProperty().bind(fx_root.widthProperty().subtract(5));
        fx_subRoot.prefHeightProperty().bind(fx_root.heightProperty().subtract(5));
        fx_menu.prefWidthProperty().bind(fx_subRoot.widthProperty());
        fx_topicLabel.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(20));
//        fx_role.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(fxLabelAIRole.widthProperty()));
        fx_scrollPane.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(20));
        fx_scrollPane.prefHeightProperty().bind(fx_subRoot.heightProperty().divide(1.8));
        fx_output.prefWidthProperty().bind(fx_scrollPane.widthProperty().subtract(20));
        fx_description.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(20));
        fxHboxButton.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(20));

//        fx_role.setFont(new Font("UTF-8", 12));

    }
    public void onUploadButtonClick(ActionEvent actionEvent) {
    }

    public void onClearButtonClick(ActionEvent actionEvent) {
    }

    public void onGenerateButtonClick() {

        try {
            dallE.generateImage(fx_description.getText(), 2, "1024x1024");
            createGridconstruct(2);
        } catch (Exception e) {
            System.out.println("hier ist was schief gelaufen");
        }
    }

    public void onEditButtonClick(ActionEvent actionEvent) {
    }

    public void onVariateButtonClick(ActionEvent actionEvent) {
    }

    public void onMenuLoadProject(ActionEvent actionEvent) {
    }

    public void onMenuSaveProject(ActionEvent actionEvent) {
    }

    public void onMenuCloseButton(ActionEvent actionEvent) {
    }

    public void onMenuAboutButton(ActionEvent actionEvent) {
    }

    private void createGridconstruct(int numGrids){
        if (numGrids < 1 || numGrids > 10)
            throw new IllegalArgumentException("Number of inserts must be between 1 and 10.");

        GridPane gp = new GridPane();
        gp.setPadding(new Insets(10));
        gp.setHgap(5.);
        gp.setVgap(5.);
        gp.setAlignment(Pos.CENTER);
        int numRows = (numGrids % 2 == 0) ? (numGrids/2) : ((numGrids/2)+1);
        int numCols = (numGrids >= 2) ? 2 : 1;
        for (int i = 0; i < numRows; i++) {
            gp.getRowConstraints().add(new RowConstraints(50));
        }
        for (int i = 0; i < numCols; i++) {
            gp.getColumnConstraints().add(new ColumnConstraints(50));
        }

        Request lastReq = dallE.getLastRequest();
        List<File> fileList = lastReq.getImageFiles();
        try {
            int index = 0;
            for (int row = 0; row < numRows; row++) {
                for (int col = 0; col < numCols; col++) {
                    ImageView imgV = new ImageView(loadImage(fileList.get(index).toString()));
                    gp.add(imgV,col,row);
                    index++;
                }
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        fx_output.getChildren().add(gp);
    }

    private Image loadImage(String filename) throws IOException {
        InputStream is = Files.newInputStream(Paths.get(filename));
        Image image = new Image(is, fx_output.getWidth()/2,fx_output.getHeight()/2,true,true );
        return image;
    }
}
*/
