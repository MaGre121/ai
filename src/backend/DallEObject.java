package backend;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import handler.ConnectionHandler;
import handler.FileHandler;
import handler.JsonHandler;

import java.net.URL;
import java.util.*;
import java.util.concurrent.CountDownLatch;

/**
 * This Class creates a Dall-E-Object that holds information about the generated Images
 * It keeps the prompt, num of Images to generate, some additional Information and a Stack of URLs (maybe B64_image in future versions)
 *
 * @author Matthias Grebe
 * @version 1.0
 */
public class DallEObject {
    private String strPromp;
    private String strSize;
    private int iNum;
    private boolean boolQuality;
    private String strStyle;
    private String strResponseFormat;
    private List<URL> imgURLs;
    private List<String> revisedPrompts;
    private List<String> fileNames;
    private Map<String, String> imageInfos;
    private CountDownLatch latch;

    public DallEObject(String strPromp, String strSize, int iNum, boolean boolQuality, String strStyle, String strResponseFormat) {
        this.strPromp = strPromp;
        this.strSize = strSize;
        this.iNum = iNum;
        this.boolQuality = boolQuality;
        this.strStyle = strStyle;
        this.strResponseFormat = strResponseFormat;
        this.imgURLs = new LinkedList<>();
        this.revisedPrompts = new LinkedList<>();
        this.fileNames = new LinkedList<>();
        this.imageInfos = new HashMap<>();
        this.latch = new CountDownLatch(iNum);
        processRequests();
    }

    public DallEObject(String prompt, String size, int iNum, boolean quality, String style, String responseFormat, List<String> fileNames, List<String> revisedPrompts) {
        this.strPromp = prompt;
        this.strSize = size;
        this.iNum = iNum;
        this.boolQuality = quality;
        this.strSize = style;
        this.strResponseFormat = responseFormat;
        this.fileNames = fileNames;
        this.revisedPrompts = revisedPrompts;
        this.imageInfos = new HashMap<>();
    }

    public DallEObject(String prompt, String size, int iNum, boolean quality, String style, String responseFormat, Map<String, String> imageInfos) {
        this.strPromp = prompt;
        this.strSize = size;
        this.iNum = iNum;
        this.boolQuality = quality;
        this.strSize = style;
        this.strResponseFormat = responseFormat;
        this.fileNames = fileNames;
        this.revisedPrompts = revisedPrompts;
        this.imageInfos = imageInfos;
    }

    public void processRequests() {
        for (int i = 0; i < iNum; i++) {
            JsonObject requestBody = new JsonObject();
            requestBody.add("model", new JsonPrimitive("dall-e-3"));
            requestBody.add("prompt", new JsonPrimitive(strPromp));
            requestBody.add("size", new JsonPrimitive(strSize));
            requestBody.add("n", new JsonPrimitive(1)); // Set to 1 as per requirement
            if (boolQuality)
                requestBody.add("quality", new JsonPrimitive("hd"));
            requestBody.add("style", new JsonPrimitive(strStyle));
//            requestBody.add("responseFormat", new JsonPrimitive(strResponseFormat));

            Thread requestThread = new Thread(() -> {
                try {
                    JsonObject response = ConnectionHandler.connect(requestBody, Settings.getImageGenerationEndpoint());
                    processResponse(response);
                } finally {
                    latch.countDown();
                }
            });
            requestThread.start();
        }
    }

    private synchronized void processResponse(JsonObject response) {
        if (response != null) {
            List<JsonElement> urlList = JsonHandler.getElements(response, "url");
            List<JsonElement> revisedPromptList = JsonHandler.getElements(response, "revised_prompt");
            String krampf = "";
            String anfall = "";

            for (JsonElement jsonElement : urlList) {
                try {
                    URL url = new URL(jsonElement.getAsString());
                    imgURLs.add(url);
                    krampf = FileHandler.downloadImage(url);
                    fileNames.add(krampf);

                } catch (Exception e) {
                    // Handle exception related to URL parsing
                }

            }
            for (JsonElement jsonElement : revisedPromptList) {
                anfall = jsonElement.getAsString();
                revisedPrompts.add(anfall);
            }
            if (krampf != "" || anfall != "")
                imageInfos.put(krampf, anfall);

            for (Map.Entry<String, String> entry : imageInfos.entrySet()) {
                System.out.println(entry.getKey() + " " + entry.getValue());

            }

        }
    }

    public List<URL> getImgURLs() {
        try {
            latch.await();
        } catch (InterruptedException e) {
            System.err.println("something went wrong during accessing the image URLS");
        }
        return new ArrayList<>(imgURLs);
    }

    public Map<String, String> getImageInfos() {
        if (latch != null) {
            try {
                latch.await();
            } catch (InterruptedException e) {
                System.err.println("something went wrong during accessing the krampMap!");
            }
        }
        return imageInfos;
    }

    public List<String> getRevisedPrompts() {
        if (latch != null) {
            try {
                latch.await();
            } catch (InterruptedException e) {
                System.err.println("something went wrong during accessing the revised Prompts");
            }
        }
        return revisedPrompts;
    }

    public List<String> getFileNames() {
        if (latch != null) {
            try {
                latch.await();
            } catch (InterruptedException e) {
                System.err.println("something went wrong during accessing the file Names Stack");
            }
        }
        return fileNames;
    }

    public String getPrompt() {
        return strPromp;
    }

    public String getSize() {
        return strSize;
    }

    public int getN() {
        return iNum;
    }

    public boolean getQuality() {
        return boolQuality;
    }

    public String getStyle() {
        return strStyle;
    }

    public String getResponseFormat() {
        return strResponseFormat;
    }

    public void replaceFileName(String oldFilename, String newFileName) {
        for (int i = 0; i < fileNames.size(); i++) {
            if (fileNames.get(i).equals(oldFilename)) {
                fileNames.set(i, newFileName);
            }
        }

    }
}
