package backend;

import com.google.gson.*;

public class ChatObject {
    //Body Element
    private JsonObject requestBody;
    //Message Array including all Objects and subObjects
    private JsonArray messages;
    private JsonObject systemMessage;
    private JsonObject userMessage;
    private JsonObject assistantMessage;
    private JsonObject toolMessage;
    private JsonObject functionMessage;
    private String model;
    private int frequency_penalty;
    private int max_tokens;
    private int n;
    private int presence_penalty;
    private JsonObject response_format;
    private int seed;
    private String[] stop;
    private boolean stream;
    private int temperature;
    private int top_p;
    private JsonArray tools;
    private String toolsType;
    private JsonObject toolsFunction;
    private String toolsFunctionDescription;
    private String toolsFunctionName;
    private JsonObject toolsFunctionParameters;
    private JsonObject tool_choice;
    private String user;

    public ChatObject() {
        requestBody = new JsonObject();
        messages = new JsonArray();
        model = Settings.getModel();
        build();
    }

    public void build() {
        requestBody.add("model", new JsonPrimitive(model));
        requestBody.add("messages", messages);
        changeSystemMessage("You are a helpfull assistant!");
    }

    public void changeSystemMessage(String message) {
        systemMessage = new JsonObject();
        systemMessage.addProperty("role", "system");
        systemMessage.addProperty("content", message);
        try {
            messages.set(0, systemMessage);
        } catch (IndexOutOfBoundsException e) {
            messages.add(systemMessage);
        }
    }

    public void addUserMessage(String message) {
        userMessage = new JsonObject();
        userMessage.addProperty("role", "user");
        userMessage.addProperty("content", message);
        messages.add(userMessage);
    }

    public void addAssistantMessage(JsonObject jsonObject){
        assistantMessage = jsonObject;
        messages.add(assistantMessage);
    }

    public void addAssistantMessage(String message){
        assistantMessage = new JsonObject();
        assistantMessage.addProperty("role", "assistant");
        assistantMessage.addProperty("content", message);
        messages.add(assistantMessage);
    }

    public String getLastMessage(){
        return assistantMessage.get("content").getAsString();
    }

    public String getRole(){
        return messages.get(0).getAsJsonObject().get("content").getAsString();
    }

    public JsonObject asJsonObject(){
        return requestBody;
    }

    public JsonArray getMessages(){
        return messages;
    }

    @Override
    public String toString() {
        return requestBody.toString();
    }
}
