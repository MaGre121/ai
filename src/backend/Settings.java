package backend;

import com.google.gson.JsonElement;
import handler.ConnectionHandler;
import handler.JsonHandler;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * The Settings class provides methods for creating, saving, and reading the properties file.
 * It also provides methods to access and manipulate settings related to Proxy and ChatGPT.
 *  @author Matthias Grebe
 *  @version 1.1
 *
 *  20231201
 *  - added Dall-E parameters
 *  - only Dall-E-3 support
 */
public class Settings {
    private static final String path = System.getProperty("user.home") + "//.chatgpt/settings.prop";
    private static final String defaultModel = "gpt-3.5-turbo";
    private static Properties properties = new Properties();
    private static List<String> searchList = null;
    private static Proxy proxy;

    private static final String APIKEY = "cgpt.apikey";
    private static final String MODEL = "cgpt.model";
    private static final String PROXYHOST = "proxy.hostname";
    private static final String PROXYPORT = "proxy.port";
    private static final String PROXYUSER = "proxy.user";
    private static final String PROXYPASS = "proxy.password";

    private static final String CHATENDPOINT="api.chat.endpoint";
    private static final String IMAGEGENERATIONENDPOINT="api.imagegeneration.endpoint";
    private static final String IMAGEEDITENNDPOINT="api.imageedit.endpoint";
    private static final String IMAGEVARIATIONENDPOINT="api.imagevariation.endpoint";
    private static final String MODELSENDPOINT="api.models.endpoint";

    private static final String DALLESIZE = "dallE.size";
    private static final String DALLEQUALITY = "dallE.hd";
    private static final String DALLESTYLE = "dallE.style";
    private static final String DALLENUM = "dallE.n";

    private Settings() {
    }

    /**
     * Initializes the Settings class by checking the properties folder and loading the properties from the file.
     */
    public static void initialize() {
        checkFolder();
        try (InputStream input = Files.newInputStream(Paths.get(path))){
            properties.load(input);
            buildProxy();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void buildProxy(){
        ProxySettings proxySettings;
        if (properties.getProperty(PROXYUSER).equals("")) {
            if (properties.getProperty(PROXYHOST).equals("")) {
                proxy=null;
            } else {
                proxySettings = new ProxySettings(properties.getProperty(PROXYHOST), Integer.parseInt(properties.getProperty(PROXYPORT)));
                proxy = proxySettings.getProxy();
            }
        } else {
            proxySettings = new ProxySettings(properties.getProperty(PROXYHOST), Integer.parseInt(properties.getProperty(PROXYPORT)), properties.getProperty(PROXYUSER), properties.getProperty(PROXYPASS));
            proxy = proxySettings.getProxy();
        }
    }

    /**
     * Gets the properties object.
     * @return The properties object.
     */
    public static Properties getProperties() {
        return properties;
    }

    /**
     * Gets the value of a specific property.
     * @param string The property key.
     * @return The property value.
     */
    public static String getProperty(String string){
        return properties.getProperty(string);
    }

    /**
     * Retrieves the Proxy settings as a Proxy object.
     * @return The Proxy object representing the Proxy settings.
     */
    public static Proxy getProxy(){
        return proxy;
    }

    /**
     * Retrieves the API key for ChatGPT.
     * @return The API key for ChatGPT.
     */
    public static String getApiKey(){
        return properties.getProperty(APIKEY);
    }

    /**
     * Retrieves the model ID for ChatGPT.
     * @return The model ID for ChatGPT.
     */
    public static String getModel(){
        return properties.getProperty(MODEL);
    }
    public static String getChatendpoint() {
        return properties.getProperty(CHATENDPOINT);
    }
    public static String getImageGenerationEndpoint(){
        return properties.getProperty(IMAGEGENERATIONENDPOINT);
    }
    public static String getImagevariationendpoint(){
        return properties.getProperty(IMAGEVARIATIONENDPOINT);
    }
    public static String getImageeditenndpoint(){
        return properties.getProperty(IMAGEEDITENNDPOINT);
    }
    public static String getDallesize() { return properties.getProperty(DALLESIZE);}
    public static boolean getDallequality(){
        return Boolean.parseBoolean(properties.getProperty(DALLEQUALITY));
    }
    public static String getDallestyle(){return properties.getProperty(DALLESTYLE);}
    public static int getDallenum(){return Integer.parseInt(properties.getProperty(DALLENUM));}


    /**
     * Retrieves the default URL for ChatGPT API.
     * @return The default URL for ChatGPT API.
     */
    public static String getModelsEndpoint(){
        return properties.getProperty(MODELSENDPOINT);
    }

    /**
     * Writes the properties to the properties file.
     * @param apikey The API key for ChatGPT.
     * @param username The username for proxy authentication.
     * @param password The password for proxy authentication.
     * @param hostname The hostname of the proxy.
     * @param port The port number of the proxy.
     */
    public static void writeProperties(String apikey, String username, String password, String hostname, int port){
        writeProperties(apikey, defaultModel, username, password, hostname, 80);
    }

    /**
     * Writes the properties to the properties file.
     * @param apikey The API key for ChatGPT.
     * @param model The model ID for ChatGPT.
     * @param username The username for proxy authentication.
     * @param password The password for proxy authentication.
     * @param hostname The hostname of the proxy.
     * @param port The port number of the proxy.
     */
    public static void writeProperties(String apikey,String model, String username, String password, String hostname, int port){

        try (OutputStream out = Files.newOutputStream(Paths.get(path))) {
            Properties prop = properties;

            prop.setProperty(APIKEY, apikey);
            prop.setProperty(MODEL, model);
            prop.setProperty(PROXYUSER, username);
            prop.setProperty(PROXYPASS, password);
            prop.setProperty(PROXYHOST, hostname);
            prop.setProperty(PROXYPORT, String.valueOf(port));
            prop.store(out, null);
            initialize();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static void writeDallePoperties(String size, String style, boolean hd, int n){
        try (OutputStream out = Files.newOutputStream(Paths.get(path))) {
            Properties prop = properties;

            prop.setProperty(DALLESIZE, size);
            prop.setProperty(DALLESTYLE, style);
            prop.setProperty(DALLEQUALITY, String.valueOf(hd));
            prop.setProperty(DALLENUM, String.valueOf(n));
            prop.store(out, null);
            initialize();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    /**
     * Updates the properties by re-initializing the Settings class.
     */
    public static void update(){
        initialize();
    }

    private static void checkFolder(){
        Path userhome = Paths.get(Settings.path).getParent();
        try  {
            Files.createDirectory(userhome);
            properties.setProperty(IMAGEGENERATIONENDPOINT, "https://api.openai.com/v1/images/generations");
            properties.setProperty(IMAGEEDITENNDPOINT, "https://api.openai.com/v1/images/edits");
            properties.setProperty(IMAGEVARIATIONENDPOINT, "https://api.openai.com/v1/images/variations");
            properties.setProperty(CHATENDPOINT, "https://api.openai.com/v1/chat/completions");
            properties.setProperty(MODELSENDPOINT, "https://api.openai.com/v1/models");

            writeProperties("", defaultModel, "", "", "", 80);
            writeDallePoperties("1024x1024", "natural", false, 2);
        } catch (IOException e) {
//            System.out.println("folder already exists, going on without recreating");
        }
    }

    /**
     * Retrieves the available models from OpenAI's models API.
     * @return A list of available models.
     */
    public static List<String> getModels() {
        List<String> modelList = new ArrayList<>();
        List<JsonElement> searchList = JsonHandler.getElements(ConnectionHandler.connect(null,properties.getProperty(MODELSENDPOINT),false, "GET"), "id");

        for (JsonElement s : searchList) {
            String st = s.getAsString();
            if (st.contains("gpt-") || st.contains("dall-e"))
                modelList.add(st);
        }
        return modelList;
    }

    /**
     * The ProxySettings class represents the Proxy settings including hostname, port, username, and password.
     */
    private static class ProxySettings {
        private Proxy proxy;

        /**
         * Constructs a ProxySettings object with the specified hostname, port, username, and password.
         * @param hostname The hostname of the proxy.
         * @param port The port number of the proxy.
         * @param username The username for proxy authentication.
         * @param password The password for proxy authentication.
         */
        public ProxySettings(String hostname, int port, String username, String password) {
            this.proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(hostname, port));
            enableProxyWithCredentials();
            Authenticator.setDefault(new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return (new PasswordAuthentication(username, password.toCharArray()));
                }
            });
        }

        /**
         * Constructs a ProxySettings object with the specified hostname and port.
         * @param hostname  The hostname of the proxy.
         * @param port The port number of the proxy.
         */
        public ProxySettings(String hostname, int port) {
            this.proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(hostname, port));
        }

        /**
         * Constructs a ProxySettings object with default values (empty hostname and port 80).
         */
        public ProxySettings() {
            this("", 80);
        }

        private void enableProxyWithCredentials(){
            System.setProperty("jdk.http.auth.tunneling.disabledSchemes", "");
            System.setProperty("jdk.http.auth.proxying.disabledSchemes", "");
        }

        /**
         * Retrieves the Proxy object representing the Proxy settings.
         * @return The Proxy object.
         */
        public Proxy getProxy() {
            return this.proxy;
        }
    }
}