package backend.enums;

public enum DalleSizes {
    SIZE_1024x1024("1024x1024"),
    SIZE_1792x1024("1792x1024"),
    SIZE_1024x1792("1024x1792");

    private final String displayValue;

    DalleSizes(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }
}
