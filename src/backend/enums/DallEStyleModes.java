package backend.enums;

public enum DallEStyleModes {
    NATURAL("natural"),
    VIVID("vivid");

    private final String displayValue;

    DallEStyleModes(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }
}
