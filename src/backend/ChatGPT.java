package backend;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import handler.ConnectionHandler;
import handler.FileHandler;
import handler.JsonHandler;

import java.util.List;


/**
 * @author Matthias Grebe
 * @version 2.0
 **/


public class ChatGPT {
    private JsonArray answers;
    private ChatObject cObj;

    public ChatGPT() {
        cObj = new ChatObject();
        this.answers = new JsonArray(); //falls man noch was mit den Antworten machen will
    }

    public void askChatGPT(String messageContent) {
        this.cObj.addUserMessage(messageContent);
        JsonObject answer = ConnectionHandler.connect(cObj.asJsonObject(), Settings.getChatendpoint());
        cObj.addAssistantMessage(JsonHandler.getElements(answer, "message").get(0).getAsJsonObject());
        answers.add(answer);
    }

    public String getAnswer() {
        return cObj.getLastMessage();
    }

    public void clearHistory() {
        cObj = new ChatObject();
    }

    public void exitProgram() {
        System.exit(0);
    }

    public String getRole() {
        return cObj.getRole();
    }

    public void changeRole(String rolle) {
        cObj.changeSystemMessage(rolle);
    }

    public String saveConversation() {
        return FileHandler.saveConversationAsHTML(cObj.getMessages());
    }

    public int getUsedTokens() {
        try {
            return answers.get(answers.size() - 1).getAsJsonObject().get("usage").getAsJsonObject().get("total_tokens").getAsInt();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<String> getModels() {
        return Settings.getModels();

    }
}
