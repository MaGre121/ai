package backend;

import handler.FileHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Matthias Grebe
 * @version 1.0
 * This Class is used to generate Dall-E-3 Images only
 * Imageformat will be URL-Download (passed to ConnectionHandler.downloadImage(Url))
 * or base64_json conversion (passed to FileHandler.saveBase64Image(Base64String))
 **/


public class DallE {
    //      ImageFormat
    private final String imgFormat = "url";
    List<DallEObject> dallEObjects;


    public DallE() {
        dallEObjects = new LinkedList<>();
    }

    public List<DallEObject> getDallEObjects() {
        return dallEObjects;
    }

    public DallEObject getLatestObject(){
        return dallEObjects.get(dallEObjects.size()-1);
    }

    public void generateImage(String prompt, boolean savinActive) {
        DallEObject dallEObject = new DallEObject(prompt, Settings.getDallesize(), Settings.getDallenum(), Settings.getDallequality(), Settings.getDallestyle(), imgFormat);
        dallEObjects.add(dallEObject);

        if (savinActive)
            saveActiveImage();
//        return createGridconstruct(dallEObject);
    }

    public void loadProject(File projectFile) {
        dallEObjects.clear();
        Document doc = FileHandler.loadProject(projectFile);

        NodeList nList = doc.getElementsByTagName("DallEObject");
        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                String prompt = eElement.getElementsByTagName("Prompt").item(0).getTextContent();
                String size = eElement.getElementsByTagName("size").item(0).getTextContent();
                int n = Integer.parseInt(eElement.getElementsByTagName("n").item(0).getTextContent());
                boolean quality = Boolean.parseBoolean(eElement.getElementsByTagName("hd").item(0).getTextContent());
                String style = eElement.getElementsByTagName("style").item(0).getTextContent();
                String responseFormat = eElement.getElementsByTagName("response_format").item(0).getTextContent();
                List<String> fileNames = new ArrayList<>();
                List<String> revisedPrompts = new ArrayList<>();
                Map<String, String> imageInfos = new HashMap<>();

                Element replyElement = (Element) eElement.getElementsByTagName("Reply").item(0);
                NodeList images = replyElement.getElementsByTagName("image");
                NodeList revPrompts = replyElement.getElementsByTagName("revisedPrompt");
                if (images.getLength() == n && revPrompts.getLength() == n)
                    for (int j = 0; j < n; j++) {
                        fileNames.add(images.item(j).getTextContent());
                        revisedPrompts.add(revPrompts.item(j).getTextContent());
                        imageInfos.put(images.item(j).getTextContent(), revPrompts.item(j).getTextContent());
                    }
                dallEObjects.add(new DallEObject(prompt, size, n, quality, style, responseFormat, imageInfos));
            }
        }
    }

    //WARNING! DO NOT CALL THIS FUNCTION WHEN savingActive IS FALSE!!
    public void saveAllImages() {
        if (dallEObjects.size() != 0)
            for (DallEObject dallEObject : dallEObjects) {
                FileHandler.moveImagesFromTempToProject(dallEObject);
                FileHandler.addImagesToProjectXML(dallEObject);
            }
    }

    public void saveActiveImage() {
        FileHandler.addImagesToProjectXML(dallEObjects.get(dallEObjects.size() - 1));
    }


    /*public GridPane createGridconstruct(DallEObject dallEObject) {

        int numGrids = dallEObject.getN();
        if (numGrids < 1 || numGrids > 10)
            throw new IllegalArgumentException("Number of inserts must be between 1 and 10.");

        GridPane gp = new GridPane();
        gp.setPadding(new Insets(10));
        gp.setHgap(5.);
        gp.setVgap(5.);
        gp.setAlignment(Pos.CENTER);
        TextField header = new TextField(dallEObject.getPrompt());

        *//*int numRows = (numGrids % 2 == 0) ? (numGrids/2) : ((numGrids/2)+1);
        int numCols = (numGrids >= 2) ? 2 : 1;*//*
        int numCols = 3; // Immer 3 Bilder pro Reihe
        gp.add(header, 0, 0, numCols, 1);
        int numRows = (numGrids / 3) + (numGrids % 3 == 0 ? 0 : 1); // Dividiere durch 3 und füge bei Bedarf eine zusätzliche Zeile hinzu


        try {
            int index = 0;
            for (int row = 0; row < numRows; row++) {
                for (int col = 0; col < numCols; col++) {
                    if (index < numGrids) {
                        Path filename = Paths.get(dallEObject.getFileNames().get(index));

//                        String strImage = FileHandler.loadImage(filename);
                        String imageFile = filename.toUri().toString();
                        ImageView imgV = new ImageView(new Image(imageFile));
//                        ImageView imgV = new ImageView(FileHandler.loadImage(filename));
                        int finalIndex = index;
                        AtomicBoolean active = new AtomicBoolean(false);
                        imgV.setOnMouseClicked(event -> {

                            if (event.getClickCount() == 2) {
//                                openImageInNewWindow(fileList.get(finalIndex).toString());
                            } else if (event.getClickCount() == 1) {
//                                resetresizes(gp);
//                                if (!active.get()) {
//                                    tmpImgView = fileList.get(finalIndex).toString();
//                                    imgV.setScaleX(1.3);
//                                    imgV.setScaleY(1.3);
//                                    imgV.toFront();
//                                    active.set(true);
                            } else if (active.get()) {
//                                    resetresizes(gp);
                            }
//                                fx_description.setText(lastReq.getPrompt());
                        });
                        gp.add(imgV, col, row);
                        index++;
                    } else
                        break;
                }
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        return gp;
    }*/

    public Map<Image, String> getlatestImages() {
        Map<String, String> imageInfos = dallEObjects.get(dallEObjects.size()-1).getImageInfos();
        Map<Image, String> outMap = new HashMap<>();

        for (Map.Entry<String, String> entry : imageInfos.entrySet()) {
            String filenameURI = Paths.get(entry.getKey()).toUri().toString();
            outMap.put(new Image(filenameURI), entry.getValue());
        }
        return outMap;
    }

    public Map<Image, String> getImageFromObject(DallEObject dallEObject) {
        Map<String, String> imageInfos = dallEObject.getImageInfos();
        Map<Image, String> outMap = new HashMap<>();

        for (Map.Entry<String, String> entry : imageInfos.entrySet()) {
            String filenameURI = Paths.get(entry.getKey()).toUri().toString();
            outMap.put(new Image(filenameURI), entry.getValue());
        }
        return outMap;
    }
}